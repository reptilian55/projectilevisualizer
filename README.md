# ProjectileVisualizer (WIP)
LUA-based projectile visualization script for TF2.

## Installation
1. Download [lnxLib](https://github.com/lnx00/Lmaobox-Library/releases/download/v1.00.0/lnxLib.lua)
2. Download [ProjectileVisualizer](https://gitlab.com/darth_lane/projectilevisualizer/-/raw/main/ProjectileVisualizer.lua?inline=false)
3. Place both in %localappdata%
4. Run script in-game