
--[[  Movement assist for  Lmaobox  ]]--
--[[           --Author--           ]]--
--[[           Terminator           ]]--
--[[  (github.com/titaniummachine1  ]]--

---@alias AimTarget { entity : Entity, angles : EulerAngles, factor : number }

---@type boolean, lnxLib
local libLoaded, lnxLib = pcall(require, "lnxLib")
assert(libLoaded, "lnxLib not found, please install it!")
assert(lnxLib.GetVersion() >= 0.995, "lnxLib version is too old, please update it!")
UnloadLib() --unloads all packages

local Math, Conversion = lnxLib.Utils.Math, lnxLib.Utils.Conversion
local WPlayer, WWeapon = lnxLib.TF2.WPlayer, lnxLib.TF2.WWeapon
local Helpers = lnxLib.TF2.Helpers
local Prediction = lnxLib.TF2.Prediction
local Fonts = lnxLib.UI.Fonts

local config = {
	polygon = {
		enabled = true;
		r = 255;
		g = 200;
		b = 155;
		a = 50;

		size = 10;
		segments = 20;
	};
	
	line = {
		enabled = true;
		r = 255;
		g = 255;
		b = 255;
		a = 255;
	};

	flags = {
		enabled = false;
		r = 255;
		g = 0;
		b = 0;
		a = 255;

		size = 5;
	};

	outline = {
		line_and_flags = false;
		polygon = true;
		r = 0;
		g = 0;
		b = 0;
		a = 155;
	};

	-- 0.5 to 8, determines the size of the segments traced, lower values = worse performance (default 2.5)
	measure_segment_size = 2.5;
};

local function NormalizeVector(vector)
    local length = math.sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
    if length == 0 then
        return Vector3(0, 0, 0)
    else
        return Vector3(vector.x / length, vector.y / length, vector.z / length)
    end
end
--[[
local function calculateAveragePosition(pointsList)
    local totalPoints = #pointsList
    local sumX, sumY, sumZ = 0, 0, 0

    for _, point in ipairs(pointsList) do
        sumX = sumX + point.x
        sumY = sumY + point.y
        sumZ = sumZ + point.z
    end

    return { x = sumX / totalPoints, y = sumY / totalPoints, z = sumZ / totalPoints }
end

local function findEffectivePosition(position, pointsList)
    local averagePosition = calculateAveragePosition(pointsList)
    local directionVector = (averagePosition - position)
    local magnitude = directionVector:Length()

    if magnitude >= 50 then
        return position -- If the current position is already 50 units away from the average position, no need to move.
    else
        local targetPosition = { x = position.x + directionVector.x / magnitude * 50, 
                                 y = position.y + directionVector.y / magnitude * 50,
                                 z = position.z + directionVector.z / magnitude * 50 }
        return targetPosition
    end
end]]

local CROSS = (function(a, b, c) return (b[1] - a[1]) * (c[2] - a[2]) - (b[2] - a[2]) * (c[1] - a[1]); end);
local CLAMP = (function(a, b, c) return (a<b) and b or (a>c) and c or a; end);
local TRACE_HULL = engine.TraceHull;
local WORLD2SCREEN = client.WorldToScreen;
local POLYGON = draw.TexturedPolygon;
local LINE = draw.Line;
local COLOR = draw.Color;
local M_RADPI = 180 / math.pi
local FORWARD_COLLISION_ANGLE = 55
local GROUND_COLLISION_ANGLE_LOW = 45
local GROUND_COLLISION_ANGLE_HIGH = 55
local MAX_PROJECTILE_SIZE = 30

local aItemDefinitions = {};
do
	local laDefinitions = {
        [44] 	= 12;		--The Sandman                                   tf_weapon_bat_wood
		[222]	= 11;		--Mad Milk                                      tf_weapon_jar_milk
        [648] 	= 12;		--The Wrap Assassin                             tf_weapon_bat_giftwrap
		[812]	= 12;		--The Flying Guillotine                         tf_weapon_cleaver
		[833]	= 12;		--The Flying Guillotine (Genuine)               tf_weapon_cleaver
		[1121]	= 11;		--Mutated Milk                                  tf_weapon_jar_milk

		[18]	= -1;		--Rocket Launcher                               tf_weapon_rocketlauncher
		[205]	= -1;		--Rocket Launcher (Renamed/Strange)             tf_weapon_rocketlauncher
		[127]	= -1;		--The Direct Hit                                tf_weapon_rocketlauncher_directhit
		[228]	= -1;		--The Black Box                                 tf_weapon_rocketlauncher
		[237]	= -1;		--Rocket Jumper                                 tf_weapon_rocketlauncher
		[414]	= -1;		--The Liberty Launcher                          tf_weapon_rocketlauncher
		[441]	= -1;		--The Cow Mangler 5000                          tf_weapon_particle_cannon	
		[513]	= -1;		--The Original                                  tf_weapon_rocketlauncher
		[658]	= -1;		--Festive Rocket Launcher                       tf_weapon_rocketlauncher
		[730]	= -1;		--The Beggar's Bazooka                          tf_weapon_rocketlauncher
		[800]	= -1;		--Silver Botkiller Rocket Launcher Mk.I         tf_weapon_rocketlauncher
		[809]	= -1;		--Gold Botkiller Rocket Launcher Mk.I           tf_weapon_rocketlauncher
		[889]	= -1;		--Rust Botkiller Rocket Launcher Mk.I           tf_weapon_rocketlauncher
		[898]	= -1;		--Blood Botkiller Rocket Launcher Mk.I          tf_weapon_rocketlauncher
		[907]	= -1;		--Carbonado Botkiller Rocket Launcher Mk.I      tf_weapon_rocketlauncher
		[916]	= -1;		--Diamond Botkiller Rocket Launcher Mk.I        tf_weapon_rocketlauncher
		[965]	= -1;		--Silver Botkiller Rocket Launcher Mk.II        tf_weapon_rocketlauncher
		[974]	= -1;		--Gold Botkiller Rocket Launcher Mk.II          tf_weapon_rocketlauncher
		[1085]	= -1;		--Festive Black Box                             tf_weapon_rocketlauncher
		[1104]	= -1;		--The Air Strike                                tf_weapon_rocketlauncher_airstrike
		[15006]	= -1;		--Woodland Warrior                              tf_weapon_rocketlauncher
		[15014]	= -1;		--Sand Cannon                                   tf_weapon_rocketlauncher
		[15028]	= -1;		--American Pastoral                             tf_weapon_rocketlauncher
		[15043]	= -1;		--Smalltown Bringdown                           tf_weapon_rocketlauncher
		[15052]	= -1;		--Shell Shocker                                 tf_weapon_rocketlauncher
		[15057]	= -1;		--Aqua Marine                                   tf_weapon_rocketlauncher
		[15081]	= -1;		--Autumn                                        tf_weapon_rocketlauncher
		[15104]	= -1;		--Blue Mew                                      tf_weapon_rocketlauncher
		[15105]	= -1;		--Brain Candy                                   tf_weapon_rocketlauncher
		[15129]	= -1;		--Coffin Nail                                   tf_weapon_rocketlauncher
		[15130]	= -1;		--High Roller's                                 tf_weapon_rocketlauncher
		[15150]	= -1;		--Warhawk                                       tf_weapon_rocketlauncher

		[442]	= -1;		--The Righteous Bison                           tf_weapon_raygun

		[1178]	= -1;		--Dragon's Fury                                 tf_weapon_rocketlauncher_fireball

		[39]	= 8;		--The Flare Gun                                 tf_weapon_flaregun
		[351]	= 8;		--The Detonator                                 tf_weapon_flaregun
		[595]	= 8;		--The Manmelter                                 tf_weapon_flaregun_revenge
		[740]	= 8;		--The Scorch Shot                               tf_weapon_flaregun
		[1180]	= 11;		--The Gas Passer                                tf_weapon_jar_gas

		[19]	= 5;		--Grenade Launcher                              tf_weapon_grenadelauncher
		[206]	= 5;		--Grenade Launcher (Renamed/Strange)            tf_weapon_grenadelauncher
		[308]	= 5;		--The Loch-n-Load                               tf_weapon_grenadelauncher
		[996]	= 6;		--The Loose Cannon                              tf_weapon_cannon
		[1007]	= 5;		--Festive Grenade Launcher                      tf_weapon_grenadelauncher
		[1151]	= 4;		--The Iron Bomber                               tf_weapon_grenadelauncher
		[15077]	= 5;		--Autumn                                        tf_weapon_grenadelauncher
		[15079]	= 5;		--Macabre Web                                   tf_weapon_grenadelauncher
		[15091]	= 5;		--Rainbow                                       tf_weapon_grenadelauncher
		[15092]	= 5;		--Sweet Dreams                                  tf_weapon_grenadelauncher
		[15116]	= 5;		--Coffin Nail                                   tf_weapon_grenadelauncher
		[15117]	= 5;		--Top Shelf                                     tf_weapon_grenadelauncher
		[15142]	= 5;		--Warhawk                                       tf_weapon_grenadelauncher
		[15158]	= 5;		--Butcher Bird                                  tf_weapon_grenadelauncher

		[20]	= 1;		--Stickybomb Launcher                           tf_weapon_pipebomblauncher
		[207]	= 1;		--Stickybomb Launcher (Renamed/Strange)         tf_weapon_pipebomblauncher
		[130]	= 3;		--The Scottish Resistance                       tf_weapon_pipebomblauncher
		[265]	= 3;		--Sticky Jumper                                 tf_weapon_pipebomblauncher
		[661]	= 1;		--Festive Stickybomb Launcher                   tf_weapon_pipebomblauncher
		[797]	= 1;		--Silver Botkiller Stickybomb Launcher Mk.I     tf_weapon_pipebomblauncher
		[806]	= 1;		--Gold Botkiller Stickybomb Launcher Mk.I       tf_weapon_pipebomblauncher
		[886]	= 1;		--Rust Botkiller Stickybomb Launcher Mk.I       tf_weapon_pipebomblauncher
		[895]	= 1;		--Blood Botkiller Stickybomb Launcher Mk.I      tf_weapon_pipebomblauncher
		[904]	= 1;		--Carbonado Botkiller Stickybomb Launcher Mk.I  tf_weapon_pipebomblauncher
		[913]	= 1;		--Diamond Botkiller Stickybomb Launcher Mk.I    tf_weapon_pipebomblauncher
		[962]	= 1;		--Silver Botkiller Stickybomb Launcher Mk.II    tf_weapon_pipebomblauncher
		[971]	= 1;		--Gold Botkiller Stickybomb Launcher Mk.II      tf_weapon_pipebomblauncher
		[1150]	= 2;		--The Quickiebomb Launcher                      tf_weapon_pipebomblauncher
		[15009]	= 1;		--Sudden Flurry                                 tf_weapon_pipebomblauncher
		[15012]	= 1;		--Carpet Bomber                                 tf_weapon_pipebomblauncher
		[15024]	= 1;		--Blasted Bombardier                            tf_weapon_pipebomblauncher
		[15038]	= 1;		--Rooftop Wrangler                              tf_weapon_pipebomblauncher
		[15045]	= 1;		--Liquid Asset                                  tf_weapon_pipebomblauncher
		[15048]	= 1;		--Pink Elephant                                 tf_weapon_pipebomblauncher
		[15082]	= 1;		--Autumn                                        tf_weapon_pipebomblauncher
		[15083]	= 1;		--Pumpkin Patch                                 tf_weapon_pipebomblauncher
		[15084]	= 1;		--Macabre Web                                   tf_weapon_pipebomblauncher
		[15113]	= 1;		--Sweet Dreams                                  tf_weapon_pipebomblauncher
		[15137]	= 1;		--Coffin Nail                                   tf_weapon_pipebomblauncher
		[15138]	= 1;		--Dressed to Kill                               tf_weapon_pipebomblauncher
		[15155]	= 1;		--Blitzkrieg                                    tf_weapon_pipebomblauncher

        [528]   = -1;		--The Short Circuit                             tf_weapon_mechanical_arm
		[588]	= -1;		--The Pomson 6000                               tf_weapon_drg_pomson
		[997]	= 9;		--The Rescue Ranger                             tf_weapon_shotgun_building_rescue

		[17]	= 10;		--Syringe Gun                                   tf_weapon_syringegun_medic
		[204]	= 10;		--Syringe Gun (Renamed/Strange)                 tf_weapon_syringegun_medic
		[36]	= 10;		--The Blutsauger                                tf_weapon_syringegun_medic
		[305]	= 9;		--Crusader's Crossbow                           tf_weapon_crossbow
		[412]	= 10;		--The Overdose                                  tf_weapon_syringegun_medic
		[1079]	= 9;		--Festive Crusader's Crossbow                   tf_weapon_crossbow

		[56]	= 7;		--The Huntsman                                  tf_weapon_compound_bow
		[1005]	= 7;		--Festive Huntsman                              tf_weapon_compound_bow
		[1092]	= 7;		--The Fortified Compound                        tf_weapon_compound_bow

		[58]	= 11;		--Jarate                                        tf_weapon_jar
		[1083]	= 11;		--Festive Jarate                                tf_weapon_jar
		[1105]	= 11;		--The Self-Aware Beauty Mark                    tf_weapon_jar
	};

	local iHighestItemDefinitionIndex = 0;
	for i, _ in pairs(laDefinitions) do
		iHighestItemDefinitionIndex = math.max(iHighestItemDefinitionIndex, i);
	end

	for i = 1, iHighestItemDefinitionIndex do
		table.insert(aItemDefinitions, laDefinitions[i] or false)
	end
end

local PhysicsEnvironment = physics.CreateEnvironment();
do
	PhysicsEnvironment:SetGravity( Vector3( 0, 0, -800 ) )
	PhysicsEnvironment:SetAirDensity( 2.0 )
	PhysicsEnvironment:SetSimulationTimestep(1/66)
end

local PhysicsObjectHandler = {};
do
	PhysicsObjectHandler.m_aObjects = {};
	PhysicsObjectHandler.m_iActiveObject = 0;

	function PhysicsObjectHandler:Initialize()
		if #self.m_aObjects > 0 then
			return;
		end

		local function new(path)
			local solid, model = physics.ParseModelByName(path);
			table.insert(self.m_aObjects, PhysicsEnvironment:CreatePolyObject(model, solid:GetSurfacePropName(), solid:GetObjectParameters()));
		end

		new("models/weapons/w_models/w_stickybomb.mdl");										--Stickybomb
		new("models/workshop/weapons/c_models/c_kingmaker_sticky/w_kingmaker_stickybomb.mdl");	--QuickieBomb
		new("models/weapons/w_models/w_stickybomb_d.mdl");										--ScottishResistance, StickyJumper
		
		self.m_aObjects[1]:Wake();
		self.m_iActiveObject = 1;
	end

	function PhysicsObjectHandler:Destroy()
		self.m_iActiveObject = 0;
		
		if #self.m_aObjects == 0 then
			return;
		end
		
		for i, obj in pairs(self.m_aObjects) do
			PhysicsEnvironment:DestroyObject(obj)
			self.m_aObjects[i] = nil;
		end
	end

	setmetatable(PhysicsObjectHandler, {
		__call = function(self, iRequestedObject)
			if iRequestedObject ~= self.m_iActiveObject then
				self.m_aObjects[self.m_iActiveObject]:Sleep();
				self.m_aObjects[iRequestedObject]:Wake();

				self.m_iActiveObject = iRequestedObject;
			end
			
			return self.m_aObjects[self.m_iActiveObject];
		end;
	});
end

local TrajectoryLine = {};
do
	TrajectoryLine.m_aPositions = {};
	TrajectoryLine.m_iSize = 0;
	TrajectoryLine.m_vFlagOffset = Vector3(0, 0, 0);

	function TrajectoryLine:Insert(vec)
		self.m_iSize = self.m_iSize + 1;
		self.m_aPositions[self.m_iSize] = vec;
	end

	local iLineRed,    iLineGreen,    iLineBlue,    iLineAlpha    = config.line.r,    config.line.g,    config.line.b,    config.line.a;
	local iFlagRed,    iFlagGreen,    iFlagBlue,    iFlagAlpha    = config.flags.r,   config.flags.g,   config.flags.b,   config.flags.a;
	local iOutlineRed, iOutlineGreen, iOutlineBlue, iOutlineAlpha = config.outline.r, config.outline.g, config.outline.b, config.outline.a;
	local iOutlineOffsetInner = (config.flags.size < 1) and -1 or 0;
	local iOutlineOffsetOuter = (config.flags.size < 1) and -1 or 1;

	local metatable = {__call = nil;};
	if not config.line.enabled and not config.flags.enabled then
		function metatable:__call() end
	
	elseif config.outline.line_and_flags then
		if config.line.enabled and config.flags.enabled then
			function metatable:__call()
				local positions, offset, last = self.m_aPositions, self.m_vFlagOffset, nil;
				
				COLOR(iOutlineRed, iOutlineGreen, iOutlineBlue, iOutlineAlpha);
				for i = self.m_iSize, 1, -1 do
					local this_pos = positions[i];
					local new, newf = WORLD2SCREEN(this_pos), WORLD2SCREEN(this_pos + offset);
				
					if last and new then
						if math.abs(last[1] - new[1]) > math.abs(last[2] - new[2]) then
							LINE(last[1], last[2] - 1, new[1], new[2] - 1);
							LINE(last[1], last[2] + 1, new[1], new[2] + 1);

						else
							LINE(last[1] - 1, last[2], new[1] - 1, new[2]);
							LINE(last[1] + 1, last[2], new[1] + 1, new[2]);
						end
					end

					if new and newf then
						LINE(newf[1], newf[2] - 1, new[1], new[2] - 1);
						LINE(newf[1], newf[2] + 1, new[1], new[2] + 1);
						LINE(newf[1] - iOutlineOffsetOuter, newf[2] - 1, newf[1] - iOutlineOffsetOuter, newf[2] + 2);
					end
					
					last = new;
				end

				last = nil;

				for i = self.m_iSize, 1, -1 do
					local this_pos = positions[i];
					local new, newf = WORLD2SCREEN(this_pos), WORLD2SCREEN(this_pos + offset);
				
					if last and new then
						COLOR(iLineRed, iLineGreen, iLineBlue, iLineAlpha);
						LINE(last[1], last[2], new[1], new[2]);
					end

					if new and newf then
						COLOR(iFlagRed, iFlagGreen, iFlagBlue, iFlagAlpha);
						LINE(newf[1], newf[2], new[1], new[2]);
					end
					
					last = new;
				end
			end

		elseif config.line.enabled then
			function metatable:__call()
				local positions, offset, last = self.m_aPositions, self.m_vFlagOffset, nil;
				
				for i = self.m_iSize, 1, -1 do
					local this_pos = positions[i];
					local new = WORLD2SCREEN(this_pos);
				
					if last and new then
						COLOR(iOutlineRed, iOutlineGreen, iOutlineBlue, iOutlineAlpha);
						if math.abs(last[1] - new[1]) > math.abs(last[2] - new[2]) then
							LINE(last[1], last[2] - 1, new[1], new[2] - 1);
							LINE(last[1], last[2] + 1, new[1], new[2] + 1);

						else
							LINE(last[1] - 1, last[2], new[1] - 1, new[2]);
							LINE(last[1] + 1, last[2], new[1] + 1, new[2]);
						end

						COLOR(iLineRed, iLineGreen, iLineBlue, iLineAlpha);
						LINE(last[1], last[2], new[1], new[2]);
					end
					
					last = new;
				end
			end

		else
			function metatable:__call()
				local positions, offset = self.m_aPositions, self.m_vFlagOffset;
				
				for i = self.m_iSize, 1, -1 do
					local this_pos = positions[i];
					local new, newf = WORLD2SCREEN(this_pos), WORLD2SCREEN(this_pos + offset);
				
					if new and newf then
						COLOR(iOutlineRed, iOutlineGreen, iOutlineBlue, iOutlineAlpha);
						LINE(new[1] + iOutlineOffsetInner, new[2] - 1, new[1] + iOutlineOffsetInner, new[2] + 2);
						LINE(newf[1], newf[2] - 1, new[1], new[2] - 1);
						LINE(newf[1], newf[2] + 1, new[1], new[2] + 1);
						LINE(newf[1] - iOutlineOffsetOuter, newf[2] - 1, newf[1] - iOutlineOffsetOuter, newf[2] + 2);

						COLOR(iFlagRed, iFlagGreen, iFlagBlue, iFlagAlpha);
						LINE(newf[1], newf[2], new[1], new[2]);
					end
				end
			end
		end

	elseif config.line.enabled and config.flags.enabled then
		function metatable:__call()
			local positions, offset, last = self.m_aPositions, self.m_vFlagOffset, nil;
				
			for i = self.m_iSize, 1, -1 do
				local this_pos = positions[i];
				local new, newf = WORLD2SCREEN(this_pos), WORLD2SCREEN(this_pos + offset);
				
				if last and new then
					COLOR(iLineRed, iLineGreen, iLineBlue, iLineAlpha);
					LINE(last[1], last[2], new[1], new[2]);
				end

				if new and newf then
					COLOR(iFlagRed, iFlagGreen, iFlagBlue, iFlagAlpha);
					LINE(newf[1], newf[2], new[1], new[2]);
				end
					
				last = new;
			end
		end

	elseif config.line.enabled then
		function metatable:__call()
			local positions, last = self.m_aPositions, nil;
			
			COLOR(iLineRed, iLineGreen, iLineBlue, iLineAlpha);
			for i = self.m_iSize, 1, -1 do
				local new = WORLD2SCREEN(positions[i]);
				
				if last and new then
					LINE(last[1], last[2], new[1], new[2]);
				end
					
				last = new;
			end
		end

	else
		function metatable:__call()
			local positions, offset = self.m_aPositions, self.m_vFlagOffset;
			
			COLOR(iFlagRed, iFlagGreen, iFlagBlue, iFlagAlpha);
			for i = self.m_iSize, 1, -1 do
				local this_pos = positions[i];
				local new, newf = WORLD2SCREEN(this_pos), WORLD2SCREEN(this_pos + offset);
				
				if new and newf then
					LINE(newf[1], newf[2], new[1], new[2]);
				end
			end
		end
	end

	setmetatable(TrajectoryLine, metatable);
end

local ImpactPolygon = {};
do
	ImpactPolygon.m_iTexture = draw.CreateTextureRGBA(string.char(0xff, 0xff, 0xff, config.polygon.a, 0xff, 0xff, 0xff, config.polygon.a, 0xff, 0xff, 0xff, config.polygon.a, 0xff, 0xff, 0xff, config.polygon.a), 2, 2);

	local iSegments = config.polygon.segments;
	local fSegmentAngleOffset = math.pi / iSegments;
	local fSegmentAngle = fSegmentAngleOffset * 2;

	local metatable = { __call = function(self, plane, origin) end; };
	if config.polygon.enabled then

		if config.outline.polygon then
			function metatable:__call(plane, origin)
				local positions = {};
				local radius = config.polygon.size;

				if math.abs(plane.z) >= 0.99 then
					for i = 1, iSegments do
						local ang = i * fSegmentAngle + fSegmentAngleOffset;
						positions[i] = WORLD2SCREEN(origin + Vector3(radius * math.cos(ang), radius * math.sin(ang), 0));
						if not positions[i] then
							return;
						end
					end
	
				else
					local right = Vector3(-plane.y, plane.x, 0);
					local up = Vector3(plane.z * right.y, -plane.z * right.x, (plane.y * right.x) - (plane.x * right.y));

					radius = radius / math.cos(math.asin(plane.z))

					for i = 1, iSegments do
						local ang = i * fSegmentAngle + fSegmentAngleOffset;
						positions[i] = WORLD2SCREEN(origin + (right * (radius * math.cos(ang))) + (up * (radius * math.sin(ang))));

						if not positions[i] then
							return;
						end
					end
				end

				COLOR(config.outline.r, config.outline.g, config.outline.b, config.outline.a);
				local last = positions[#positions];
				for i = 1, #positions do
					local new = positions[i]

					if math.abs(new[1] - last[1]) > math.abs(new[2] - last[2]) then
						LINE(last[1], last[2] + 1, new[1], new[2] + 1);
						LINE(last[1], last[2] - 1, new[1], new[2] - 1);
					else
						LINE(last[1] + 1, last[2], new[1] + 1, new[2]);
						LINE(last[1] - 1, last[2], new[1] - 1, new[2]);
					end


					last = new;
				end

				COLOR(config.polygon.r, config.polygon.g, config.polygon.b, 255);
				do
					local cords, reverse_cords = {}, {};
					local sizeof = #positions;
					local sum = 0;

					for i, pos in pairs(positions) do
						local convertedTbl = {pos[1], pos[2], 0, 0};

						cords[i], reverse_cords[sizeof - i + 1] = convertedTbl, convertedTbl;

						sum = sum + CROSS(pos, positions[(i % sizeof) + 1], positions[1]);
					end


					POLYGON(self.m_iTexture, (sum < 0) and reverse_cords or cords, true)
				end

				local last = positions[#positions];
				for i = 1, #positions do
					local new = positions[i];
				
					LINE(last[1], last[2], new[1], new[2]);

					last = new;
				end

			end

		else
			function metatable:__call(plane, origin)
				local positions = {};
				local radius = config.polygon.size;

				if math.abs(plane.z) >= 0.99 then
					for i = 1, iSegments do
						local ang = i * fSegmentAngle + fSegmentAngleOffset;
						positions[i] = WORLD2SCREEN(origin + Vector3(radius * math.cos(ang), radius * math.sin(ang), 0));
						if not positions[i] then
							return;
						end
					end
	
				else
					local right = Vector3(-plane.y, plane.x, 0);
					local up = Vector3(plane.z * right.y, -plane.z * right.x, (plane.y * right.x) - (plane.x * right.y));

					radius = radius / math.cos(math.asin(plane.z))

					for i = 1, iSegments do
						local ang = i * fSegmentAngle + fSegmentAngleOffset;
						positions[i] = WORLD2SCREEN(origin + (right * (radius * math.cos(ang))) + (up * (radius * math.sin(ang))));

						if not positions[i] then
							return;
						end
					end
				end

				COLOR(config.polygon.r, config.polygon.g, config.polygon.b, 255);
				do
					local cords, reverse_cords = {}, {};
					local sizeof = #positions;
					local sum = 0;

					for i, pos in pairs(positions) do
						local convertedTbl = {pos[1], pos[2], 0, 0};

						cords[i], reverse_cords[sizeof - i + 1] = convertedTbl, convertedTbl;

						sum = sum + CROSS(pos, positions[(i % sizeof) + 1], positions[1]);
					end


					POLYGON(self.m_iTexture, (sum < 0) and reverse_cords or cords, true)
				end

				local last = positions[#positions];
				for i = 1, #positions do
					local new = positions[i];
				
					LINE(last[1], last[2], new[1], new[2]);

					last = new;
				end

			end
		end
	end

	setmetatable(ImpactPolygon, metatable);
end

local GetProjectileInfo = (function()
	local aOffsets = {
		Vector3(16, 8, -6),
		Vector3(23.5, -8, -3),
		Vector3(23.5, 12, -3),
		Vector3(16, 6, -8)
	};

	local aCollisionMaxs = {
		Vector3(0, 0, 0),
		Vector3(1, 1, 1),
		Vector3(2, 2, 2),
		Vector3(3, 3, 3)
	};

	return function(pLocal, bDucking, iCase, iDefIndex, iWepID)
		local fChargeBeginTime = 0;
        if pLocal then
            fChargeBeginTime = (pLocal:GetPropFloat("PipebombLauncherLocalData", "m_flChargeBeginTime") or 0);
        end

		if fChargeBeginTime ~= 0 then
			fChargeBeginTime = globals.CurTime() - fChargeBeginTime;
		end

		if iCase == -1 then -- RocketLauncher, DragonsFury, Pomson, Bison
			local vOffset, vCollisionMax, fForwardVelocity = Vector3(23.5, -8, bDucking and 8 or -3), aCollisionMaxs[2], 0;
			
			if iWepID == 22 or iWepID == 65 then
				vOffset.y, vCollisionMax, fForwardVelocity = (iDefIndex == 513) and 0 or 12, aCollisionMaxs[1], (iWepID == 65) and 2000 or (iDefIndex == 414) and 1550 or 1100;

			elseif iWepID == 109 then
				vOffset.y, vOffset.z = 6, -3;

			else
				fForwardVelocity = 1200;
			end
			
			return vOffset, fForwardVelocity, 0, vCollisionMax, 0;

		elseif iCase == 1  then return aOffsets[1], 900 + CLAMP(fChargeBeginTime / 4, 0, 1) * 1500, 200, aCollisionMaxs[3], 0;                                   -- StickyBomb
		elseif iCase == 2  then return aOffsets[1], 900 + CLAMP(fChargeBeginTime / 1.2, 0, 1) * 1500, 200, aCollisionMaxs[3], 0;                                 -- QuickieBomb
		elseif iCase == 3  then return aOffsets[1], 900 + CLAMP(fChargeBeginTime / 4, 0, 1) * 1500, 200, aCollisionMaxs[3], 0;                                   -- ScottishResistance, StickyJumper
		elseif iCase == 4  then return aOffsets[1], 1200, 200, aCollisionMaxs[3], 400, 0.45;                                                                     -- TheIronBomber
		elseif iCase == 5  then return aOffsets[1], (iDefIndex == 308) and 1500 or 1200, 200, aCollisionMaxs[3], 400, (iDefIndex == 308) and 0.225 or 0.45;      -- GrenadeLauncher, LochnLoad
		elseif iCase == 6  then return aOffsets[1], 1440, 200, aCollisionMaxs[3], 560, 0.5;                                                                      -- LooseCannon
		elseif iCase == 7  then return aOffsets[2], 1800 + CLAMP(fChargeBeginTime, 0, 1) * 800, 0, aCollisionMaxs[2], 200 - CLAMP(fChargeBeginTime, 0, 1) * 160; -- Huntsman
		elseif iCase == 8  then return Vector3(23.5, 12, bDucking and 8 or -3), 2000, 0, aCollisionMaxs[1], 120;                                                 -- FlareGuns
		elseif iCase == 9  then return aOffsets[2], 2400, 0, aCollisionMaxs[((iDefIndex == 997) and 2 or 4)], 80;                                                -- CrusadersCrossbow, RescueRanger
		elseif iCase == 10 then return aOffsets[4], 1000, 0, aCollisionMaxs[2], 120;                                                                             -- SyringeGuns
		elseif iCase == 11 then return Vector3(23.5, 8, -3), 1000, 200, aCollisionMaxs[4], 450;                                                                  -- Jarate, MadMilk
		elseif iCase == 12 then return Vector3(23.5, 8, -3), 3000, 300, aCollisionMaxs[3], 900, 1.3;                                                             -- FlyingGuillotine
		end
	end
end)();

local g_fTraceInterval = CLAMP(config.measure_segment_size, 0.5, 8) / 66;
local g_fFlagInterval = g_fTraceInterval * 1320;
local function isNaN(x) return x ~= x end
local vHitbox = { Vector3(-24, -24, 0), Vector3(24, 24, 82) }
local aInfoProjectiles = {}

function GetEntityMoveAngles(entity, overrideVelocity)
    local vVelocity = entity:GetPropVector("m_vInitialVelocity")

    if overrideVelocity then
        vVelocity = overrideVelocity
    end

    local vNormalizedVelocity = NormalizeVector(vVelocity)

    local pitch = math.atan(vNormalizedVelocity.z / math.sqrt(vNormalizedVelocity.x^2 + vNormalizedVelocity.y^2)) * M_RADPI
    local yaw = math.atan(vNormalizedVelocity.y / vNormalizedVelocity.x) * M_RADPI

    -- Fix pitch and yaw
    pitch = -pitch
    if vNormalizedVelocity.x < 0 then
        yaw = yaw + 180
    end
    if yaw < 0 then
        yaw = yaw + 360
    end

    if isNaN(pitch) then pitch = 0 end
    if isNaN(yaw) then yaw = 0 end

    return EulerAngles(pitch, yaw, 0)
end

-- Weapons with projectiles that either have no initial velocity or no projectile type
local AltWeaponIndices = {
    [44] = true,
    [442] = true,
    [528] = true,
    [588] = true,
    [648] = true,
    [1178] = true
}

local function GetIsValidAltWeapon(pWeapon)
    if pWeapon and pWeapon:IsValid() then
        local iItemDefinitionIndex = pWeapon:GetPropInt("m_iItemDefinitionIndex")
        print("pWeapon: " .. tostring(pWeapon) .. ", ID: " .. iItemDefinitionIndex)
        if AltWeaponIndices[iItemDefinitionIndex] then
            return true
        end
    end
    return false
end

local function GetPlayerWeapon(player)
    local pWeapon = player:GetPropEntity("m_hActiveWeapon")

    if not pWeapon or (pWeapon:GetWeaponProjectileType() or 0) < 2 or pWeapon:IsMeleeWeapon() then
        if GetIsValidAltWeapon(pWeapon) then
            return pWeapon
        end
        
        local m_hMyWeapons = player:GetPropEntity("m_hMyWeapons")
        if not m_hMyWeapons or (m_hMyWeapons:GetWeaponProjectileType() or 0) < 2 then
            if GetIsValidAltWeapon(m_hMyWeapons) then
                return m_hMyWeapons
            end

            return nil
        end
        pWeapon = m_hMyWeapons
    end
    return pWeapon
end

local function GetProjectileInfoByPlayer(player, overrideIndex, overrideWepID)
    local iItemDefinitionIndex = overrideIndex

    local pWeapon = GetPlayerWeapon(player)
    if not overrideIndex then
        if not pWeapon then
            return nil
        else
            iItemDefinitionIndex = pWeapon:GetPropInt("m_iItemDefinitionIndex")
        end
    end

    local iItemDefinitionType = aItemDefinitions[iItemDefinitionIndex] or 0
    if iItemDefinitionType == 0 then
        return nil
    end

    local WepID = overrideWepID
    if not overrideWepID then
        WepID = pWeapon:GetWeaponID()
    end

    local vOffset, fForwardVelocity, fUpwardVelocity, vCollisionMax, fGravity, fDrag = GetProjectileInfo(pWeapon, (player:GetPropInt("m_fFlags") & FL_DUCKING) == 2, iItemDefinitionType, iItemDefinitionIndex, WepID)
    return iItemDefinitionType, -vCollisionMax, vOffset, fForwardVelocity, fUpwardVelocity, vCollisionMax, fGravity, fDrag
end

function GetEscapePosition(entity, projectile)
    local startPos, endPos = GetProjectilePath(projectile)
    local escapePosition = nil

    local tTraceResult = engine.TraceLine(startPos, endPos, MASK_SHOT_HULL)

    if tTraceResult.fraction ~= 1 and tTraceResult.entity == entity then
        local entityPosition = entity:GetAbsOrigin()

        -- The player is a box
        if entity:IsPlayer() then
            local bounds = entity:GetCollisionBounds()
            print("bounds", bounds)
        end
    end

    return escapePosition
end

local function HandleGroundCollision(groundTrace)
    local vUp = Vector3(0, 0, 1)
    local normal = groundTrace.plane
    local angle = math.deg(math.acos(normal:Dot(vUp)))
    local onGround = false
    if angle < GROUND_COLLISION_ANGLE_LOW then
        onGround = true
    elseif angle < GROUND_COLLISION_ANGLE_HIGH then
        onGround = false
    else
        onGround = true
    end
    return groundTrace.endpos, onGround
end

local function IsProjectileFinished(projectile)
    local startPos = projectile:GetAbsOrigin()
    local posCount = #aInfoProjectiles[projectile:GetIndex()].vPositions
    local endPos = aInfoProjectiles[projectile:GetIndex()].vPositions[posCount]
    if endPos then
        -- If projectile within small range of end position
        if (endPos - startPos):Length() < 50 then
            return true
        end
    end
    return false
end

local function GetEntityOwner(entity)
    local owner = entity:GetPropEntity("m_hOwnerEntity")
    local parent = entity:GetPropEntity("m_hThrower")
    local builder = entity:GetPropEntity("m_hBuilder")
    if not owner or owner:GetName() == nil or owner:IsDormant() then
        if not parent or parent:GetName() == nil or parent:IsDormant() then
            if not builder or builder:GetName() == nil or builder:IsDormant() then
                return nil
            end
            return builder
        end
        return parent
    end
    return owner
end

local function GetProjectiles()
    -- Get all valid projectiles
    local projectiles = {}
    local lunchboxes = entities.FindByClass("CBaseAnimating")
    for _, entity in pairs(lunchboxes) do
        local owner = entity:GetPropEntity("m_hOwnerEntity")
        if owner and owner:IsPlayer() then
            table.insert(projectiles, entity)
        end
    end
    local arrows = entities.FindByClass("CTFProjectile_Arrow")
    for _, entity in pairs(arrows) do
        table.insert(projectiles, entity)
    end
    local ornaments = entities.FindByClass("CTFBall_Ornament")
    for _, entity in pairs(ornaments) do
        table.insert(projectiles, entity)
    end
    local bof = entities.FindByClass("CTFProjectile_BallOfFire")
    for _, entity in pairs(bof) do
        table.insert(projectiles, entity)
    end
    local cleavers = entities.FindByClass("CTFProjectile_Cleaver")
    for _, entity in pairs(cleavers) do
        table.insert(projectiles, entity)
    end
    local energyballs = entities.FindByClass("CTFProjectile_EnergyBall")
    for _, entity in pairs(energyballs) do
        table.insert(projectiles, entity)
    end
    local energyrings = entities.FindByClass("CTFProjectile_EnergyRing")
    for _, entity in pairs(energyrings) do
        table.insert(projectiles, entity)
    end
    local flares = entities.FindByClass("CTFProjectile_Flare")
    for _, entity in pairs(flares) do
        table.insert(projectiles, entity)
    end
    local grapples = entities.FindByClass("CTFProjectile_GrapplingHook")
    for _, entity in pairs(grapples) do
        table.insert(projectiles, entity)
    end
    local healbolts = entities.FindByClass("CTFProjectile_HealingBolt")
    for _, entity in pairs(healbolts) do
        table.insert(projectiles, entity)
    end
    local jar = entities.FindByClass("CTFProjectile_Jar")
    for _, entity in pairs(jar) do
        table.insert(projectiles, entity)
    end
    local jargas = entities.FindByClass("CTFProjectile_JarGas")
    for _, entity in pairs(jargas) do
        table.insert(projectiles, entity)
    end
    local jarmilk = entities.FindByClass("CTFProjectile_JarMilk")
    for _, entity in pairs(jarmilk) do
        table.insert(projectiles, entity)
    end
    local splightorb = entities.FindByClass("CTFProjectile_SpellLightningOrb")
    for _, entity in pairs(splightorb) do
        table.insert(projectiles, entity)
    end
    local mecharmorb = entities.FindByClass("CTFProjectile_MechanicalArmOrb")
    for _, entity in pairs(mecharmorb) do
        table.insert(projectiles, entity)
    end
    local pipes = entities.FindByClass("CTFGrenadePipebombProjectile")
    for _, entity in pairs(pipes) do
        table.insert(projectiles, entity)
    end
    local rockets = entities.FindByClass("CTFProjectile_Rocket")
    for _, entity in pairs(rockets) do
        table.insert(projectiles, entity)
    end
    local sentryrockets = entities.FindByClass("CTFProjectile_SentryRocket")
    for _, entity in pairs(sentryrockets) do
        table.insert(projectiles, entity)
    end
    local spbats = entities.FindByClass("CTFProjectile_SpellBats")
    for _, entity in pairs(spbats) do
        table.insert(projectiles, entity)
    end
    local spfireball = entities.FindByClass("CTFProjectile_SpellFireball")
    for _, entity in pairs(spfireball) do
        table.insert(projectiles, entity)
    end
    local spkartbats = entities.FindByClass("CTFProjectile_SpellKartBats")
    for _, entity in pairs(spkartbats) do
        table.insert(projectiles, entity)
    end
    local spkartorb = entities.FindByClass("CTFProjectile_SpellKartOrb")
    for _, entity in pairs(spkartorb) do
        table.insert(projectiles, entity)
    end
    local spmeteorsh = entities.FindByClass("CTFProjectile_SpellMeteorShower")
    for _, entity in pairs(spmeteorsh) do
        table.insert(projectiles, entity)
    end
    local spmirv = entities.FindByClass("CTFProjectile_SpellMirv")
    for _, entity in pairs(spmirv) do
        table.insert(projectiles, entity)
    end
    local sppumpkin = entities.FindByClass("CTFProjectile_SpellPumpkin")
    for _, entity in pairs(sppumpkin) do
        table.insert(projectiles, entity)
    end
    local spspawnboss = entities.FindByClass("CTFProjectile_SpellSpawnBoss")
    for _, entity in pairs(spspawnboss) do
        table.insert(projectiles, entity)
    end
    local spspawnhorde = entities.FindByClass("CTFProjectile_SpellSpawnHorde")
    for _, entity in pairs(spspawnhorde) do
        table.insert(projectiles, entity)
    end
    local spspawnzombie = entities.FindByClass("CTFProjectile_SpellSpawnZombie")
    for _, entity in pairs(spspawnzombie) do
        table.insert(projectiles, entity)
    end
    local sptransposetp = entities.FindByClass("CTFProjectile_SpellTransposeTeleport")
    for _, entity in pairs(sptransposetp) do
        table.insert(projectiles, entity)
    end
    local stunballs = entities.FindByClass("CTFStunBall")
    for _, entity in pairs(stunballs) do
        table.insert(projectiles, entity)
    end
    local syringes = entities.FindByClass("CTFBaseProjectile")
    for _, entity in pairs(syringes) do
        table.insert(projectiles, entity)
    end
    local throwables = entities.FindByClass("CTFProjectile_Throwable")
    for _, entity in pairs(throwables) do
        table.insert(projectiles, entity)
    end
    local throwbread = entities.FindByClass("CTFProjectile_ThrowableBreadMonster")
    for _, entity in pairs(throwbread) do
        table.insert(projectiles, entity)
    end
    local throwbrick = entities.FindByClass("CTFProjectile_ThrowableBrick")
    for _, entity in pairs(throwbrick) do
        table.insert(projectiles, entity)
    end
    local throwrepel = entities.FindByClass("CTFProjectile_ThrowableRepel")
    for _, entity in pairs(throwrepel) do
        table.insert(projectiles, entity)
    end

    return projectiles
end

local function ClearInfoProjectiles()
    for id, _ in pairs(aInfoProjectiles) do
        projectile = entities.GetByIndex(id)
        if not projectile or not projectile:IsValid() or projectile:IsDormant() then
            aInfoProjectiles[id] = nil
        end
    end
end

local function CreateInfoProjectile(index)
    local info = {
        bFinished = false,
        vPositions = {},
        tImpactTrace = nil
    }

    aInfoProjectiles[index] = info

    return info
end

local function GetInfoProjectile(index)
    if not aInfoProjectiles[index] then
        return CreateInfoProjectile(index)
    end
    return aInfoProjectiles[index]
end

local function IsValidDrawingState()
    local pLocal = entities.GetLocalPlayer();
	if (engine.Con_IsVisible() or engine.IsGameUIVisible() or not pLocal or pLocal:InCond(7) or not pLocal:IsAlive()) then
        return false
    end

    return true
end

function UpdateProjectilePath(projectile, owner, lastPositionIndex)
    local iIndex = projectile:GetIndex();
    local aInfoProjectile = GetInfoProjectile(iIndex);

    local overrideIndex = nil
    local overrideWepID = nil
    if projectile:GetClass() == "CTFProjectile_SentryRocket" then
        overrideIndex = 18
        overrideWepID = TF_WEAPON_ROCKETLAUNCHER
    elseif projectile:GetClass() == "CBaseAnimating" then
        overrideIndex = 20
        overrideWepID = TF_WEAPON_PIPEBOMBLAUNCHER
    end

    local iItemDefinitionType, vCollisionMin, vOffset, fForwardVelocity, fUpwardVelocity, vCollisionMax, fGravity, fDrag = GetProjectileInfoByPlayer(owner, overrideIndex, overrideWepID)

    if iItemDefinitionType == nil then
        print("Error: iItemDefinitionType is nil")
        return
    end

    local vStartPosition = projectile:GetAbsOrigin()
    -- Override start position
    if next(aInfoProjectile.vPositions) == nil then
        table.insert(aInfoProjectile.vPositions, vStartPosition)
    else
        vStartPosition = aInfoProjectile.vPositions[1]
    end

    local vVelocity = projectile:GetPropVector("m_vInitialVelocity")
    
    local iItemDefinitionIndex = overrideIndex
    local bWeaponFlipped = false
    if not overrideIndex then
        local pWeapon = GetPlayerWeapon(owner)
        iItemDefinitionIndex = pWeapon:GetPropInt("m_iItemDefinitionIndex")
        bWeaponFlipped = pWeapon:IsViewModelFlipped()
    end

    -- In cases where initial velocity is invalid, we can fallback to estimation
    if not vVelocity or (vVelocity.x < 1 and vVelocity.y < 1 and vVelocity.z < 1
        and AltWeaponIndices[iItemDefinitionIndex]) then
        print("Fallback to estimation")
        vVelocity = projectile:EstimateAbsVelocity()
    end

    local vStartAngle = GetEntityMoveAngles(projectile, vVelocity)

    -- Start position and angle by owner
    local vStartPositionOwner = owner:GetAbsOrigin() + owner:GetPropVector("localdata", "m_vecViewOffset[0]")
    local vOwnerAngles = owner:GetPropVector("tfnonlocaldata", "m_angEyeAngles[0]")
    local vStartAngleOwner = EulerAngles(vOwnerAngles.x, vOwnerAngles.y, vOwnerAngles.z)
    local tTraceResult = TRACE_HULL(vStartPositionOwner, vStartPositionOwner + (vStartAngleOwner:Forward() * vOffset.x) + (vStartAngleOwner:Right() * (vOffset.y * (bWeaponFlipped and -1 or 1))) + (vStartAngleOwner:Up() * vOffset.z), vCollisionMin, vCollisionMax, 100679691)

    TrajectoryLine.m_vFlagOffset = vStartAngle:Right() * -config.flags.size;

    if iItemDefinitionType == -1 then
        tTraceResult = TRACE_HULL(vStartPosition, vStartPosition + (vStartAngle:Forward() * 10000), vCollisionMin, vCollisionMax, 100679691);
    
        if tTraceResult.startsolid then
            return
        end
    
        local iSegments = math.floor((tTraceResult.endpos - tTraceResult.startpos):Length() / g_fFlagInterval);
		local vForward = vStartAngle:Forward();
			
		for i = 1, iSegments do
            local vNextPosition = vForward * (i * g_fFlagInterval) + vStartPosition
            table.insert(aInfoProjectile.vPositions, vNextPosition)
		end

        table.insert(aInfoProjectile.vPositions, tTraceResult.endpos)
    elseif iItemDefinitionType > 3 then
        local vPosition = Vector3(0, 0, 0);
		for i = 0.01515, 5, g_fTraceInterval do
			local scalar = (not fDrag) and i or ((1 - math.exp(-fDrag * i)) / fDrag);

			vPosition.x = vVelocity.x * scalar + vStartPosition.x;
			vPosition.y = vVelocity.y * scalar + vStartPosition.y;
			vPosition.z = (vVelocity.z - fGravity * i) * scalar + vStartPosition.z;

			tTraceResult = TRACE_HULL(tTraceResult.endpos, vPosition, vCollisionMin, vCollisionMax, 100679691);

            table.insert(aInfoProjectile.vPositions, tTraceResult.endpos)

			if tTraceResult.fraction ~= 1 then
                break;
            end
		end
    else
        local obj = PhysicsObjectHandler(iItemDefinitionType);

        obj:SetPosition(vStartPosition, vStartAngle, true)
        obj:SetVelocity(vVelocity, Vector3(0, 0, 0))

        for i = 2, 330 do
            tTraceResult = TRACE_HULL(tTraceResult.endpos, obj:GetPosition(), vCollisionMin, vCollisionMax, 100679691);
            table.insert(aInfoProjectile.vPositions, tTraceResult.endpos)

			if tTraceResult.fraction ~= 1 then
                break;
            end
			PhysicsEnvironment:Simulate(g_fTraceInterval);
        end

        PhysicsEnvironment:ResetSimulationClock();
    end
    
	if tTraceResult then
		aInfoProjectile.tImpactTrace = tTraceResult
	end
end

function copyTable(originalTable)
    local newTable = {}
    for key, value in pairs(originalTable) do
        newTable[key] = value
    end
    return newTable
end

local function GetFuturePath(projectile)
    local aInfoProjectile = aInfoProjectiles[projectile:GetIndex()]
    if aInfoProjectile == nil then
        return {}, 0
    end

    local vCurrentPos = projectile:GetAbsOrigin()
    local aFuturePositions = {}
    local bFound = false
    local iFirstIndex = 0
    for i, pos in ipairs(aInfoProjectile.vPositions) do
        if bFound then
            table.insert(aFuturePositions, pos)
        else
            if (pos - vCurrentPos):Length() < MAX_PROJECTILE_SIZE * 2 then
                bFound = true
                iFirstIndex = i
                table.insert(aFuturePositions, pos)
            end
        end
    end

    if iFirstIndex == 0 then
        return aInfoProjectile.vPositions, iFirstIndex
    else
        return aFuturePositions, iFirstIndex
    end
end

local function DrawProjectilePath(projectile, owner)
    TrajectoryLine.m_aPositions, TrajectoryLine.m_iSize = {}, 0;

    -- Reset current projectile info
    local vCurrentPositions = aInfoProjectiles[projectile:GetIndex()].vPositions
    aInfoProjectiles[projectile:GetIndex()].vPositions = { vCurrentPositions[1] }

    -- Update future positions
    UpdateProjectilePath(projectile, owner)
    aFuturePositions, iFirstIndex = GetFuturePath(projectile)
    
    local aInfoProjectile = aInfoProjectiles[projectile:GetIndex()]
    local firstPos = next(aInfoProjectile.vPositions)

    if aInfoProjectile == nil or firstPos == nil then
        return
    end

    -- Only draw if game in valid drawing state
	if not IsValidDrawingState() then
        return
    end

    local tImpactTrace = aInfoProjectile.tImpactTrace
    ImpactPolygon(tImpactTrace.plane, tImpactTrace.endpos)

    if next(aInfoProjectile.vPositions) == nil then
        return
    end

    -- Only draw future positions
    TrajectoryLine.m_aPositions = copyTable(aFuturePositions)
    TrajectoryLine.m_iSize = #aFuturePositions
    
    TrajectoryLine()
end

callbacks.Register("CreateMove", "LoadPhysicsObjects", function()
	callbacks.Unregister("CreateMove", "LoadPhysicsObjects")

	PhysicsObjectHandler:Initialize()

    local pLocal = entities.GetLocalPlayer()

	callbacks.Register("Draw", function()
        -- Clear old info
        ClearInfoProjectiles()

        -- Get all valid projectiles
        local projectiles = GetProjectiles()
        -- Skip if no valid projectiles
        if next(projectiles) == nil then
            return
        end

        for i, projectile in pairs(projectiles) do
            local iIndex = projectile:GetIndex()

            local owner = GetEntityOwner(projectile)
            if not owner then
                goto continue
            elseif owner:GetClass() == "CObjectSentrygun" then
                owner = GetEntityOwner(owner)
            end

            local aInfoProjectile = GetInfoProjectile(iIndex)

            -- Skip all grounded projectiles
            if not aInfoProjectile.bFinished then
                aInfoProjectile.bFinished = IsProjectileFinished(projectile)
            else
                goto continue
            end
            
            DrawProjectilePath(projectile, owner)

            ::continue::
        end
	end)
end)

--[[ Remove the menu when unloaded ]]--
local function OnUnload()                                -- Called when the script is unloaded
    PhysicsObjectHandler:Destroy();
	physics.DestroyEnvironment(PhysicsEnvironment);
    UnloadLib() --unloading lualib
    client.Command('play "ui/buttonclickrelease"', true) -- Play the "buttonclickrelease" sound
end

--[[ Unregister previous callbacks ]]--
callbacks.Unregister("Unload", "PV_Unload")                    -- Unregister the "Unload" callback
callbacks.Unregister("Draw", "PV_Draw")                        -- Unregister the "Draw" callback
--[[ Register callbacks ]]--
callbacks.Register("Unload", "PV_Unload", OnUnload)                         -- Register the "Unload" callback

--[[ Play sound when loaded ]]--
client.Command('play "ui/buttonclick"', true) -- Play the "buttonclick" sound when the script is loaded